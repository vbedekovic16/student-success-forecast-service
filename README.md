# Student Success Forecast Service · [![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)

A service that provides educational data mining feature extension for the [Edgar](https://gitlab.com/edgar-group/edgar) system. Main role of the service is to provide a way to train multiple models for a given Edgar course and choose the best performing one to make predictions on students pass/fail rate and grades. The code that provides the user interface and Edgar API extensions are in a [feature branch](https://gitlab.com/edgar-group/edgar/-/tree/dev-student-forecast) of the Edgar repository.

## Setup
### 1. Starting off
- Clone repository
- Create and fill out `development-config.js` and `production-config.js` based on the `development-config-TEMPLATE.js` file
    - `PG_CONNECTION_STRING`: should be the same as in the Edgar config
    - `JUDGE0_API_URL`: URL of the judge0 service (if judge0 code execution  strategy used)
    - `POLLING_TIME_MS`: polling time for the judge0 code execution check request
    - `CONDA_BAT_PATH_WINDOWS`: location of the conda.bat file (if on-site code execution strategy used)
    - `CONDA_BAT_PATH_LINUX`: location of the conda.bat file  (if on-site code execution strategy used)
    - `CODE_EXECUTION_STRATEGY`: `judge0` or `on-site`
    - **Note**: if the service is running in production mode it asumes the service is running in the appropriate docker container so the `CONDA_BAT_PATH` variables are ignored

### 2.A Student Success Forecast in Docker
- `docker build -t student-success-forecast-module .`
- `docker run --name student-success-forecast-module-container -p 4242:4242 student-success-forecast-module`
- **Note**: this runs with your defined production config settings

### 2.B Student Success Forecast locally
Requires node.js, npm and [Conda](https://docs.anaconda.com/miniconda/).

- Start the conda shell
- Set up env:
```bash
conda create -n student-predictions-env -y -c r -c conda-forge r-base64enc r-jsonlite r-dplyr r-caret r-e1071 r-nnet r-xgboost r-randomforest r-torch
```
- Activate the created env and start R:
```bash
conda activate student-predictions-env
R
```
- Inside R, finish torch installation and install brulee:
```R
library(torch)
install_torch()
install.packages('brulee', repos='https://cloud.r-project.org')
quit()
```
- Deactivate the env and exit the conda shell
- In the terminal, set current directory to the root of the cloned project
- `npm install`
- `npm start dev` (for Windows)

### 3. Edgar <img src="https://edgar.fer.hr/images/edgar.png" alt="FER Logo" height="75"/>

- Clone the [Edgar repository](https://gitlab.com/edgar-group/edgar) and checkout to the `dev-student-forecast` branch
- The config which you will use to run the project must contain this new variable that points Edgar to the forecasting service, like the following example:
```js
...
studentSuccessForecastServiceConnectionConfig: {
    endPoint: 'http://localhost',
    port: 4242,
},
...
```
- Now complete the Edgar setup you prefer (and setup a populated database)
- Run Edgar
- Open in browser `{your-edgar-base-url}/student-success-forecast` if logged in as a teacher or `{your-edgar-base-url}/student-success-forecast/prediction` if logged in as a student

### 4. Database
- After setting up the Edgar database, run the query from the [db init file](DATABASE_INIT.sql)


### (Optional) Judge0
If you want to use the Judge0 code execution strategy:
- Install Judge0 according to their instructions: https://github.com/judge0/judge0/blob/master/CHANGELOG.md#deployment-procedure
- When the container is running, open container terminal

- Install conda.
```bash
wget --no-check-certificate https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
bash miniconda.sh -b -p /usr/local/miniconda
rm miniconda.sh

sudo conda create -p /usr/local/student-predictions-env -y -c r -c conda-forge r-base64enc r-jsonlite r-dplyr r-caret r-e1071 r-nnet r-xgboost r-randomforest r-torch
```
- Activate the conda environment and start R.
```bash
conda activate /usr/local/student-predictions-env
sudo R
```

- Finish torch and brulee install.
```R
library(torch)
install_torch()
install.packages("brulee")
quit()
```

- Close the environment.
```bash
conda deactivate
```

## Important
The project, as is, does not provide any authentication or authorization. It is designed to be accessed through the Edgar system and not directly by users.

## Appendix
This project is part of my masters dissertation [Predicting student success in Edgar automated programming assessment system](dissertation/Diplomski_rad__Predvidanje_uspjeha_studenata_u_sustavu_za_automatsko_ocjenjivanje_programskog_koda_Edgar.pdf) at the [University of Zagreb Faculty of Electrical Engineering and Computing](https://www.fer.unizg.hr/en). The PDF of the paper in the Croatian language can be found in this repository or on [Dabar - Digital Academic Archives and Repositories](https://dabar.srce.hr/islandora/object/fer%3A12103).

### Dissertation summary
The growing importance of online learning platforms allows for a lot off enrolled students in certain courses, but in turn, teachers cannot timely recognize students who have difficulties with the learning material. As the automated programming assessment system Edgar faces the same problem, this dissertation delves into research in the field of educational data mining and creates a system for cross validation of different machine learning models for predicting student success. This system concludes that the best method for predicting success varies from course to course. A standalone adjustable success prediction module is built around this system, which can be used by any course that also uses Edgar. The module uses the Judge0 system for executing the training of the model, but if the course contains a large number of enrolled students, it is forced to execute its code locally. Finally, a look at the web interface is provided, which facilitates teachers in managing the designed module and offers visualizations for identifying and analyzing at-risk students.

**Keywords**: educational data mining, machine learning, R programming language, data processing, cross validation, SQL, Angular, D3.js, data visualization, Edgar, node.js, express.js, Judge0, Docker

<img src="https://upload.wikimedia.org/wikipedia/commons/7/78/FER_logo.jpg" alt="FER Logo" height="75"/> <img src="https://upload.wikimedia.org/wikipedia/hr/thumb/d/d2/Unizg-logo-lat.svg/271px-Unizg-logo-lat.svg.png" alt="UNIZG Logo" height="75"/>

