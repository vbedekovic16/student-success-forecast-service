const fs = require('fs');
const winston = require('winston');
const tar = require('tar');
const zl = require("zip-lib");
const config = require('./../config/config');
const axios = require('axios');

const POLLING_TIME_MS = config.POLLING_TIME_MS || 5000;

async function postCodeSubmission(base64EncodedZip) {
    // Send POST request
    const postResponse = await axios.post(`${config.JUDGE0_API_URL}/submissions/`, {
        additional_files: base64EncodedZip,
        language_id: 89
    }, {
        params: {
            base64_encoded: false,
            wait: false,
            memory_limit: 1000000,
            cpu_time_limit: 43200,
            wall_time_limit: 86400
        }
    });

    const token = postResponse.data.token;

    let statusId = 1;
    let getResponse;

    // Poll the server until the status changes
    while (statusId === 1 || statusId === 2) {
        winston.info(`Polling Judge0 for submission ${token}...`);
        await new Promise(resolve => setTimeout(resolve, POLLING_TIME_MS)); // wait for 5 seconds

        getResponse = await axios.get(`${config.JUDGE0_API_URL}/submissions/${token}?base64_encoded=false&fields=stdout,status`);
        statusId = getResponse.data.status.id;
    }
    return getResponse;
}

async function executeCode(tempFolderPath) {
    // Create a zip file with the needed files
    await zl.archiveFolder(tempFolderPath, `${tempFolderPath}.zip`);
    const zipFile = fs.readFileSync(`${tempFolderPath}.zip`);
    const base64EncodedZip = zipFile.toString('base64');

    const submissionResult = await postCodeSubmission(base64EncodedZip);

    // Extract the submissionResult tarball
    const tarballData = Buffer.from(submissionResult.data.stdout, 'base64');
    fs.writeFileSync(`${tempFolderPath}.tar.gz`, tarballData);
    await tar.x({ file: `${tempFolderPath}.tar.gz`, cwd: tempFolderPath });
}

module.exports = { executeCode };