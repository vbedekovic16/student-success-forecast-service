const cp = require('child_process');
const os = require('os');
const path = require('path');
const config = require('../config/config');

async function executeCode(tempFolderPath) {
    const isWindows = os.platform() === 'win32';
    const scriptFileName = isWindows ? 'conda-script-windows.cmd' : 'conda-script-linux.sh'
    const condaScriptPath = path.join(tempFolderPath, scriptFileName);

    const options = { cwd: tempFolderPath };

    if (isWindows) {
        command = `cmd.exe /c "${condaScriptPath}" ${config.CONDA_BAT_PATH_WINDOWS}`;
    } else {
        command = `/bin/bash "${condaScriptPath}"`;
    }

    await new Promise((resolve, reject) => {
        cp.exec(command, options, (err, stdout, stderr) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log(stdout);
                if (stderr) {
                    console.log(stderr);
                }
                resolve();
            }
        });
    });
}

module.exports = { executeCode };