const winston = require('winston');

const config = require('../config/config'); // Adjust the path as necessary
const judge0Strategy = require('./judge0-strategy');
const onSiteStrategy = require('./on-site-strategy');

async function executeCode(tempFolderPath) {
    if (config.CODE_EXECUTION_STRATEGY === 'judge0') {
        winston.info('Executing code using Judge0 strategy...');
        await judge0Strategy.executeCode(tempFolderPath);
        return ;

    } else if (config.CODE_EXECUTION_STRATEGY === 'on-site') {
        winston.info('Executing code using on-site strategy...');
        await onSiteStrategy.executeCode(tempFolderPath);
        return;

    } else {
        throw new Error('Invalid code execution strategy specified in the configuration.');
    }
}

module.exports = { executeCode };