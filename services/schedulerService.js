const schedule = require('node-schedule');
const db = require("../db").db;
const winston = require('winston');
const { executeModelTraining, executeModelPrediction } = require("../services/codeRunnerService");

async function serverRestartTrainingJobsFetching() {
    winston.info('Fetching training jobs that were scheduled before the server restart...');

    // TODO - what if the server restarts, the schedule is in the past already but the training_schedule is set to null?
    const predictionPoints = await db.query(`
        SELECT prediction_point.*, forecast_configuration.id_course, forecast_configuration.id_academic_year
            FROM student_forecast.prediction_point 
                JOIN student_forecast.forecast_configuration ON prediction_point.id_forecast_configuration = forecast_configuration.id
            WHERE training_schedule IS NOT NULL
                AND training_executed = FALSE
        `);

    for (const predictionPoint of predictionPoints) {
        const courseId = predictionPoint.id_course;
        const academicYearId = predictionPoint.id_academic_year;
        const predictionPointOrdinal = predictionPoint.ordinal_prediction_point;
        const scheduleTime = predictionPoint.training_schedule;
        scheduleModelTrainingJob(courseId, academicYearId, predictionPointOrdinal, scheduleTime);
    }
}

async function scheduleModelTrainingJob(courseId, academicYearId, predictionPointOrdinal, scheduleTime) {
    // the name is unique beacuse the database has the unique constraint on (courseId, academicYearId)
    const jobUniqueName = `${courseId}-${academicYearId}-${predictionPointOrdinal}-training-job`;
    winston.info(`Scheduling model training job at ${scheduleTime} with name: ${jobUniqueName}`);
    await updatePredictionPointTrainingSchedule(courseId, academicYearId, predictionPointOrdinal, scheduleTime);

    // cancel the job if it was already scheduled
    if (schedule.scheduledJobs[jobUniqueName]) {
        schedule.scheduledJobs[jobUniqueName].cancel();
    }

    schedule.scheduleJob(
        jobUniqueName,
        scheduleTime,
        async function () {
            winston.info(`Training job ${jobUniqueName} started...`)

            const forecast_configuration = await db.one(`
                SELECT configuration_json
                    FROM student_forecast.forecast_configuration
                    WHERE id_course = $1
                        AND id_academic_year = $2
                `, [courseId, academicYearId]);
            const config = forecast_configuration.configuration_json;

            const [
                passMetrics,
                gradeMetrics,
                ensembleMetrics,
                dataInfo,
                modelData,
                codeLogData
            ] = await executeModelTraining(config, predictionPointOrdinal);
            const passMethodRow = await db.one(`
                SELECT id FROM student_forecast.registered_method WHERE name_method = $1 AND prediction_type = 'pass'
                `, [modelData.passModelMethodName])
            const passMethodId = passMethodRow.id;

            const gradeMethodRow = await db.one(`
                SELECT id FROM student_forecast.registered_method WHERE name_method = $1 AND prediction_type = 'grade'
                `, [modelData.gradeModelMethodName])
            const gradeMethodId = gradeMethodRow.id;

            await updatePredictionPointOnTrainingCompletion(
                courseId,
                academicYearId,
                predictionPointOrdinal,
                true,
                codeLogData.codeLog,

                passMethodId,
                modelData.passModelNameWithExt,
                passMetrics.accuracy.mean === 'NA' ? null : passMetrics.accuracy.mean,
                passMetrics.kappa.mean === 'NA' ? null : passMetrics.kappa.mean,
                passMetrics.balanced_accuracy.mean === 'NA' ? null : passMetrics.balanced_accuracy.mean,
                passMetrics.f1_score.mean === 'NA' ? null : passMetrics.f1_score.mean,

                gradeMethodId,
                modelData.gradeModelNameWithExt,
                gradeMetrics.accuracy.mean === 'NA' ? null : gradeMetrics.accuracy.mean,
                gradeMetrics.kappa.mean === 'NA' ? null : gradeMetrics.kappa.mean,
                gradeMetrics.balanced_accuracy.mean === 'NA' ? null : gradeMetrics.balanced_accuracy.mean,
                gradeMetrics.f1_score.mean === 'NA' ? null : gradeMetrics.f1_score.mean,

                ensembleMetrics.accuracy.mean === 'NA' ? null : ensembleMetrics.accuracy.mean,
                ensembleMetrics.kappa.mean === 'NA' ? null : ensembleMetrics.kappa.mean,
                ensembleMetrics.balanced_accuracy.mean === 'NA' ? null : ensembleMetrics.balanced_accuracy.mean,
                ensembleMetrics.f1_score.mean === 'NA' ? null : ensembleMetrics.f1_score.mean
            );
            winston.info(`Training job ${jobUniqueName} completed successfully.`)
            console.log(passMetrics, gradeMetrics, ensembleMetrics, dataInfo, modelData, codeLogData);
        });
}

async function updatePredictionPointTrainingSchedule(courseId, academicYearId, predictionPointOrdinal, scheduleTime) {
    await db.query(`
        UPDATE student_forecast.prediction_point
            SET training_schedule = $4
            FROM student_forecast.forecast_configuration
            WHERE student_forecast.prediction_point.id_forecast_configuration = student_forecast.forecast_configuration.id
                AND forecast_configuration.id_course = $1 
                AND forecast_configuration.id_academic_year = $2 
                AND prediction_point.ordinal_prediction_point = $3
        `, [courseId, academicYearId, predictionPointOrdinal, scheduleTime]);
}

async function updatePredictionPointOnTrainingCompletion(
    courseId,
    academicYearId,
    predictionPointOrdinal,
    completionStatus,
    trainingLogFileNameWithExt,

    passMethodId,
    passModelNameWithExt,
    passAccuracy,
    passKappa,
    passBalancedAccuracy,
    passF1Score,

    gradeMethodId,
    gradeModelNameWithExt,
    gradeAccuracy,
    gradeKappa,
    gradeBalancedAccuracy,
    gradeF1Score,

    ensembleAccuracy,
    ensembleKappa,
    ensembleBalancedAccuracy,
    ensembleF1Score
) {
    await db.query(`
        UPDATE student_forecast.prediction_point
            SET 
                training_executed = $4,
                id_pass_prediction_method = $5,
                pass_model_filename_with_ext = $6,
                pass_model_accuracy = $7,
                pass_model_kappa = $8,
                pass_model_balanced_accuracy = $9,
                pass_model_f1_score = $10,
                id_grade_prediction_method = $11,
                grade_model_filename_with_ext = $12,
                grade_model_accuracy = $13,
                grade_model_kappa = $14,
                grade_model_balanced_accuracy = $15,
                grade_model_f1_score = $16,
                ensemble_accuracy = $17,
                ensemble_kappa = $18,
                ensemble_balanced_accuracy = $19,
                ensemble_f1_score = $20,
                training_output_log_filename_with_ext = $21
            FROM student_forecast.forecast_configuration
            WHERE student_forecast.prediction_point.id_forecast_configuration = student_forecast.forecast_configuration.id
                AND forecast_configuration.id_course = $1 
                AND forecast_configuration.id_academic_year = $2 
                AND prediction_point.ordinal_prediction_point = $3
        `, [
        courseId, academicYearId, predictionPointOrdinal, completionStatus,
        passMethodId, passModelNameWithExt, passAccuracy, passKappa, passBalancedAccuracy, passF1Score,
        gradeMethodId, gradeModelNameWithExt, gradeAccuracy, gradeKappa, gradeBalancedAccuracy, gradeF1Score,
        ensembleAccuracy, ensembleKappa, ensembleBalancedAccuracy, ensembleF1Score, trainingLogFileNameWithExt
    ]);
}

async function scheduleModelPredictionJob(courseId, academicYearId, predictionPointOrdinal, scheduleTime) {
    // the name is unique beacuse the database has the unique constraint on (courseId, academicYearId)
    const jobUniqueName = `${courseId}-${academicYearId}-${predictionPointOrdinal}-prediction-job`;
    winston.info(`Scheduling forecasting job at ${scheduleTime} with name: ${jobUniqueName}`);
    await updatePredictionPointPredictionSchedule(courseId, academicYearId, predictionPointOrdinal, scheduleTime);

    // cancel the job if it was already scheduled
    if (schedule.scheduledJobs[jobUniqueName]) {
        schedule.scheduledJobs[jobUniqueName].cancel();
    }

    schedule.scheduleJob(
        jobUniqueName,
        scheduleTime,
        async function () {
            winston.info(`Forecasting job ${jobUniqueName} started...`)

            const forecast_configuration = await db.one(`
                SELECT 
                    forecast_configuration.configuration_json,
                    prediction_point.prediction_point_configuration_json,

                    pass_method.id AS pass_method_id,
                    pass_method.name_method AS pass_method_name,
                    prediction_point.pass_model_filename_with_ext,

                    grade_method.id AS grade_method_id,
                    grade_method.name_method AS grade_method_name,
                    prediction_point.grade_model_filename_with_ext

                    FROM student_forecast.forecast_configuration 
                        JOIN student_forecast.prediction_point 
                            ON forecast_configuration.id = prediction_point.id_forecast_configuration
                        JOIN student_forecast.registered_method AS pass_method
                            ON prediction_point.id_pass_prediction_method = pass_method.id
                        JOIN student_forecast.registered_method AS grade_method
                            ON prediction_point.id_grade_prediction_method = grade_method.id
                    WHERE id_course = $1
                        AND id_academic_year = $2
                        AND ordinal_prediction_point = $3
                `, [courseId, academicYearId, predictionPointOrdinal]);
            const config = forecast_configuration.configuration_json;

            // Overwrites the tests id per prediction point setup for current prediction point tests and academic year
            config.selectedTests = forecast_configuration.prediction_point_configuration_json
            config.selectedYears = [academicYearId],
                config.predictionPoints = {
                    numberOfPredictionPoints: 1,
                    allOrderedTestIdsInAcademicYearSplitByPredictionPoints: {}
                }
            config.predictionPoints.allOrderedTestIdsInAcademicYearSplitByPredictionPoints[academicYearId] =
                [
                    forecast_configuration.prediction_point_configuration_json.map(test => test.id),
                    []
                ];

            const modelsInfo = {
                passMethodId: forecast_configuration.pass_method_id,
                passMethodName: forecast_configuration.pass_method_name,
                passModelFileNameWithExt: forecast_configuration.pass_model_filename_with_ext,

                gradeMethodId: forecast_configuration.grade_method_id,
                gradeMethodName: forecast_configuration.grade_method_name,
                gradeModelFileNameWithExt: forecast_configuration.grade_model_filename_with_ext
            }

            const [predictionData, codeLogData] = await executeModelPrediction(config, predictionPointOrdinal, modelsInfo);
        
            await updatePredictionPointOnPredictionCompletion(
                courseId,
                academicYearId,
                predictionPointOrdinal,
                true,
                codeLogData.codeLog
            );

            const predictionPointOnlyId = await db.one(`
                SELECT prediction_point.id
                    FROM student_forecast.prediction_point
                        JOIN student_forecast.forecast_configuration
                            ON prediction_point.id_forecast_configuration = forecast_configuration.id
                    WHERE forecast_configuration.id_course = $1
                        AND forecast_configuration.id_academic_year = $2
                        AND prediction_point.ordinal_prediction_point = $3
                `, [courseId, academicYearId, predictionPointOrdinal]);

            console.log(predictionPointOnlyId)

            await insertStudentSuccessPredictionsIntoDB(predictionPointOnlyId.id, predictionData);

            winston.info(`Forecasting job ${jobUniqueName} completed successfully.`)
            console.log(codeLogData);
        }
    );
}

async function updatePredictionPointPredictionSchedule(courseId, academicYearId, predictionPointOrdinal, scheduleTime) {
    await db.query(`
        UPDATE student_forecast.prediction_point
            SET prediction_schedule = $4
            FROM student_forecast.forecast_configuration
            WHERE prediction_point.id_forecast_configuration = forecast_configuration.id
                AND forecast_configuration.id_course = $1 
                AND forecast_configuration.id_academic_year = $2 
                AND prediction_point.ordinal_prediction_point = $3
        `, [courseId, academicYearId, predictionPointOrdinal, scheduleTime]);
}

async function updatePredictionPointOnPredictionCompletion(
    courseId,
    academicYearId,
    predictionPointOrdinal,
    completionStatus,
    predictionLogFileNameWithExt
) {
    return await db.query(`
        UPDATE student_forecast.prediction_point
            SET 
                prediction_executed = $4,
                prediction_output_log_filename_with_ext = $5
            FROM student_forecast.forecast_configuration
            WHERE prediction_point.id_forecast_configuration = forecast_configuration.id
                AND forecast_configuration.id_course = $1 
                AND forecast_configuration.id_academic_year = $2 
                AND prediction_point.ordinal_prediction_point = $3
        `, [courseId, academicYearId, predictionPointOrdinal, completionStatus, predictionLogFileNameWithExt]);
}

async function insertStudentSuccessPredictionsIntoDB(predictionPointId, predictionData) {
    await db.query(`
        INSERT INTO student_forecast.student_prediction_data (
            id_prediction_point,
            id_student,
            at_the_time_of_forecasting_score,
            predicted_pass,
            predicted_grade,
            fail_odds,
            pass_odds,
            grade_1_odds,
            grade_2_odds,
            grade_3_odds,
            grade_4_odds,
            grade_5_odds
        )
            VALUES
            ${predictionData.map(prediction => `(
                ${predictionPointId},
                ${prediction.id_student},
                ${prediction.at_the_time_score},
                ${prediction.predicted_pass},
                ${prediction.predicted_grade},
                ${prediction.fail_odds},
                ${prediction.pass_odds},
                ${prediction.grade_1_odds},
                ${prediction.grade_2_odds},
                ${prediction.grade_3_odds},
                ${prediction.grade_4_odds},
                ${prediction.grade_5_odds}
            )`).join(',')}
        `);
}

module.exports = {
    scheduleModelTrainingJob,
    serverRestartTrainingJobsFetching,
    scheduleModelPredictionJob
};