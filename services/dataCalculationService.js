const math = require('mathjs');
const globals = require('../common/globals');


function calculateDifficulty(dataArray) {
    const n = dataArray.length;

    const mean = math.mean(dataArray);
    const stdDev = math.std(dataArray, 'uncorrected'); // 'uncorrected' for sample standard deviation

    if (stdDev === 0) {
        return 0; // Return 0 when all scores are the same as a neutral value
    }

    const skewness = (n / ((n - 1) * (n - 2))) * math.sum(math.map(dataArray, x => Math.pow((x - mean) / stdDev, 3)));

    return skewness;
}

function clip(value, minValue, maxValue) {
    return Math.max(minValue, Math.min(value, maxValue));
}

function normalizeSkewness(skewness, minSkewness, maxSkewness) {
    const normalized = (skewness - minSkewness) / (maxSkewness - minSkewness);
    return clip(normalized, 0, 1);
}

function calculateStudentPerformance(studentScoresForEachTest, testInfo) {
    let studentPerformance = [];
    let currentStudentBeingProcessed = null;
    let currentStudentPerformanceEntry = null;
    let studentMaxPerformance = 0;
    for (const studentTestScore of studentScoresForEachTest) {
        if (currentStudentBeingProcessed !== studentTestScore.id_student) {
            if (currentStudentPerformanceEntry) {
                studentPerformance.push(currentStudentPerformanceEntry);
                if (currentStudentPerformanceEntry.performance_score > studentMaxPerformance) {
                    studentMaxPerformance = currentStudentPerformanceEntry.performance_score;
                }
            }

            currentStudentBeingProcessed = studentTestScore.id_student;
            currentStudentPerformanceEntry = {
                id_student: currentStudentBeingProcessed,
                performance_score: 0
            };
        }
        currentStudentPerformanceEntry.performance_score += (studentTestScore.score > 0 ? studentTestScore.score : 0) * studentTestScore.max_score * testInfo.find(test => test.id === studentTestScore.test_id).num_of_students_that_took_test;
    }
    
    studentPerformance.push(currentStudentPerformanceEntry);
    if (currentStudentPerformanceEntry.performance_score > studentMaxPerformance) {
        studentMaxPerformance = currentStudentPerformanceEntry.performance_score;
    }

    return studentPerformance.map(entry => {
        entry.performance_score = entry.performance_score / studentMaxPerformance;
        return entry;
    })
}

function calculateCorrelation(scores, performances) {
    const n = scores.length;
    
    if (n !== performances.length) {
        throw new Error("Arrays must be of the same length");
    }

    const meanScores = scores.reduce((acc, val) => acc + val, 0) / n;
    const meanPerformances = performances.reduce((acc, val) => acc + val, 0) / n;

    let sumOfProductOfDeviations = 0;
    let sumOfSquaredDeviationsScores = 0;
    let sumOfSquaredDeviationsPerformances = 0;
    
    for (let i = 0; i < n; i++) {
        const deviationScores = scores[i] - meanScores;
        const deviationPerformances = performances[i] - meanPerformances;
        
        sumOfProductOfDeviations += deviationScores * deviationPerformances;
        sumOfSquaredDeviationsScores += deviationScores * deviationScores;
        sumOfSquaredDeviationsPerformances += deviationPerformances * deviationPerformances;
    }

    return sumOfProductOfDeviations / Math.sqrt(sumOfSquaredDeviationsScores * sumOfSquaredDeviationsPerformances);
}

module.exports = {
    calculateDifficulty,
    normalizeSkewness,
    calculateStudentPerformance,
    calculateCorrelation
}