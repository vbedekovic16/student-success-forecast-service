const { 
    getStudentScores,
    getTestsForCourseYear,
    getAcademicYearsEnroledStudedntsGraduatedFromRelevantCourse
} = require('./dataGathererService');
const { 
    determineIfTestIsBeforeCutoff,
    formatTestGroupFeatures,
    addGradeToStudentData,
    addPassFlagToStudentData,
    getColumnValues,
    addColumnToData,
    getUniqueColumnValues,
    getRowsWithColumnValue
 } = require('./dataTransformerService');
 const { calculateStudentPerformance } = require('./dataCalculationService');
 const { printTxtTable, saveToCSV } = require('./dataViewService');
 const globals = require('../common/globals');
const { c } = require('tar');

function concatListsUpToIndex(listOfLists, index) {
    // Slice the list of lists up to the given index + 1
    const slicedListOfLists = listOfLists.slice(0, index + 1);

    // Concatenate the sliced list of lists
    const concatenatedList = [].concat(...slicedListOfLists);

    return concatenatedList;
}

async function generateScorePredictionDataFromConfig(config, predictionPointIndex) {
    const courseId = config.courseId;
    const yearIdArray = config.selectedYears;


    const definedGroups = config.definedGroups;
    const selectedTests = config.selectedTests;
    const maxScoreOverride = config.maxScoreCutoff;

    const predictionPoints = config.predictionPoints;

    const allTestIdsPerYear = yearIdArray.map(yearId => {
        return predictionPoints.allOrderedTestIdsInAcademicYearSplitByPredictionPoints[yearId].flat();
    });

    const allLookupTablesPerYear = {}
    yearIdArray.forEach(yearId => {
        allLookupTablesPerYear[yearId] = {}
    });


    for (const test of selectedTests) {
        allLookupTablesPerYear[test.id_academic_year][test.id] = test.group;
    }

    //console.log(allLookupTablesPerYear)
    //console.log(allTestIdsPerYear)

    
    let predictionData = [];

    for (let i = 0; i < yearIdArray.length; i++) {
        const yearId = yearIdArray[i];
        const allTestIds = allTestIdsPerYear[i];
        const lookupTable = allLookupTablesPerYear[yearId];
        const beforeCutoffTestIds = concatListsUpToIndex(predictionPoints.allOrderedTestIdsInAcademicYearSplitByPredictionPoints[yearId], predictionPointIndex);
        //console.log(beforeCutoffTestIds)

        const definedGroupsBeforePredictionPoint = definedGroups.filter(group => beforeCutoffTestIds.some(testId => lookupTable[testId] === group));
        console.log(definedGroupsBeforePredictionPoint)


        let data = await getStudentScores(courseId, yearId, allTestIds)
        data = determineIfTestIsBeforeCutoff(data, beforeCutoffTestIds)
        data = formatTestGroupFeatures(definedGroupsBeforePredictionPoint, lookupTable, yearId, data, maxScoreOverride)
        predictionData = predictionData.concat(data)

    }

    const passThreshold = config.gradeThresholds[0];
    predictionData = addGradeToStudentData(predictionData, 'course_score', 'grade', config.gradeThresholds);
    predictionData = addPassFlagToStudentData(predictionData, 'course_score', 'pass', passThreshold);

    return predictionData;
    
} 

async function generateScorePredictionDataFromConfigForAllPredictionPoints(config) {
    const courseId = config.courseId;
    const yearIdArray = config.selectedYears;


    const definedGroups = config.definedGroups;
    const selectedTests = config.selectedTests;

    const predictionPoints = config.predictionPoints;

    const allTestIdsPerYear = yearIdArray.map(yearId => {
        return predictionPoints.allOrderedTestIdsInAcademicYearSplitByPredictionPoints[yearId].flat();
    });

    const allLookupTablesPerYear = {}
    yearIdArray.forEach(yearId => {
        allLookupTablesPerYear[yearId] = {}
    });


    for (const test of selectedTests) {
        allLookupTablesPerYear[test.id_academic_year][test.id] = test.group;
    }

    //console.log(allLookupTablesPerYear)
    //console.log(allTestIdsPerYear)

    for (let predictionPoint = 0; predictionPoint < predictionPoints.numberOfPredictionPoints; predictionPoint++) {
        let predictionData = [];

        for (let i = 0; i < yearIdArray.length; i++) {
            const yearId = yearIdArray[i];
            const allTestIds = allTestIdsPerYear[i];
            const lookupTable = allLookupTablesPerYear[yearId];
            const beforeCutoffTestIds = concatListsUpToIndex(predictionPoints.allOrderedTestIdsInAcademicYearSplitByPredictionPoints[yearId], predictionPoint);
            //console.log(beforeCutoffTestIds)

            const definedGroupsBeforePredictionPoint = definedGroups.filter(group => beforeCutoffTestIds.some(testId => lookupTable[testId] === group));
            console.log(definedGroupsBeforePredictionPoint)


            let data = await getStudentScores(courseId, yearId, allTestIds)
            data = determineIfTestIsBeforeCutoff(data, beforeCutoffTestIds)
            data = formatTestGroupFeatures(definedGroupsBeforePredictionPoint, lookupTable, data)
            predictionData = predictionData.concat(data)

        }

        predictionData = addGradeToStudentData(predictionData, 'course_score', 'grade');
        predictionData = addPassFlagToStudentData(predictionData, 'course_score', 'pass');

        printTxtTable(predictionData, `${courseId}-p${predictionPoint + 1}-student-data`);
        saveToCSV(predictionData, `${courseId}-p${predictionPoint + 1}-student-data`);
    }
}

async function addStudentPerformanceForRelevantCoursesToDataForAcademicYear(courseId, yearId, dataOfSingleAcademicYear, relevantCoursesData) {
    const numberOfStudents = dataOfSingleAcademicYear.length;
    for (const relevantCourse of relevantCoursesData) {
        const relevantCourseGraduationYears = await getAcademicYearsEnroledStudedntsGraduatedFromRelevantCourse(
            courseId, 
            yearId, 
            relevantCourse.id
        )
        const studentPerformanceDictionary = {};

        for (const graduationYearEntry of relevantCourseGraduationYears) {
            const graduationYearId = graduationYearEntry.id_academic_year;
            const tests = await getTestsForCourseYear(
                relevantCourse.id, 
                graduationYearId, 
                Math.floor(numberOfStudents * relevantCourse.min_percentage_of_students_that_took_test));
            const testIdList = getColumnValues(tests, 'id');

            const studentScoresOnTests = await getStudentScores(relevantCourse.id, graduationYearId, testIdList);
            const studentPerformance = calculateStudentPerformance(studentScoresOnTests, tests);

            for (const performanceEntry of studentPerformance) {
                studentPerformanceDictionary[performanceEntry.id_student] = performanceEntry.performance_score;
            }
        }

        const columnName = relevantCourse.course_acronym + globals.CORRELATION_COURSE_PERFORMANCE_FEATURE_SUFFIX;
        dataOfSingleAcademicYear = addColumnToData(dataOfSingleAcademicYear, columnName, 0);
        for (const student of dataOfSingleAcademicYear) {
            if (studentPerformanceDictionary[student.id_student]) {
                student[relevantCourse.course_acronym + globals.CORRELATION_COURSE_PERFORMANCE_FEATURE_SUFFIX] = studentPerformanceDictionary[student.id_student];
            }
        }
    }

    return dataOfSingleAcademicYear;
}

async function addStudentPerformanceForRelevantCoursesToData(courseId, data, relevantCoursesData) {
    const uniqueYearIds = getUniqueColumnValues(data, globals.INFO_FEATURE_YEAR_ID);
    const yearIds = uniqueYearIds.sort((a, b) => b - a);

    let dataWithPastCoursePerformances = [];
    let filteredDataForSingleAcademicYear = [];

    for (const yearId of yearIds) {
        filteredDataForSingleAcademicYear = getRowsWithColumnValue(data, globals.INFO_FEATURE_YEAR_ID, yearId);
        filteredDataForSingleAcademicYear = await addStudentPerformanceForRelevantCoursesToDataForAcademicYear(courseId, yearId, filteredDataForSingleAcademicYear, relevantCoursesData);
        dataWithPastCoursePerformances = dataWithPastCoursePerformances.concat(filteredDataForSingleAcademicYear);
    }

    return dataWithPastCoursePerformances;
}

function getDefinedGroupsBeforePredictionPoint(config, predictionPointIndex) {
    const yearId = config.selectedYears[0]; // Take the first academic year because the groups are the same for all academic years
    const definedGroups = config.definedGroups;
    const predictionPoints = config.predictionPoints;
    const selectedTests = config.selectedTests.filter(test => test.id_academic_year === yearId);
    const lookupTable = {};

    // Populate lookup table for the first year
    selectedTests.forEach(test => {
        lookupTable[test.id] = test.group;
    });

    const beforeCutoffTestIds = concatListsUpToIndex(predictionPoints.allOrderedTestIdsInAcademicYearSplitByPredictionPoints[yearId], predictionPointIndex);
    const definedGroupsBeforePredictionPoint = definedGroups.filter(group => beforeCutoffTestIds.some(testId => lookupTable[testId] === group));

    return definedGroupsBeforePredictionPoint;
}

function determineStudentStatsFromData(data) {
    const uniqueYearIds = getUniqueColumnValues(data, globals.INFO_FEATURE_YEAR_ID);

    const studentSuccessStatsPerYear = [];

    uniqueYearIds.forEach(yearId => {
        const filteredDataForSingleAcademicYear = getRowsWithColumnValue(data, globals.INFO_FEATURE_YEAR_ID, yearId);

        const numberOfStudents = filteredDataForSingleAcademicYear.length;

        const grades = getColumnValues(filteredDataForSingleAcademicYear, 'grade');
        const passes = getColumnValues(filteredDataForSingleAcademicYear, 'pass');

        studentSuccessStatsPerYear.push({
            id_year: yearId,
            number_of_students: numberOfStudents,

            failed_students: passes.filter(pass => pass == 0).length,
            passed_students: passes.filter(pass => pass == 1).length,

            grade_1_students: grades.filter(grade => grade == 1).length,
            grade_2_students: grades.filter(grade => grade == 2).length,
            grade_3_students: grades.filter(grade => grade == 3).length,
            grade_4_students: grades.filter(grade => grade == 4).length,
            grade_5_students: grades.filter(grade => grade == 5).length
        });
    });

    studentSuccessStatsPerYear.sort((a, b) => a.yearId - b.yearId);

    return studentSuccessStatsPerYear;
}

module.exports = {
    generateScorePredictionDataFromConfig,
    addStudentPerformanceForRelevantCoursesToData,
    getDefinedGroupsBeforePredictionPoint,
    determineStudentStatsFromData
}