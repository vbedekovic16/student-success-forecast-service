#!/bin/bash
. /opt/conda/bin/activate

conda activate student-predictions-env

Rscript on-site-script.R

conda deactivate

conda deactivate
