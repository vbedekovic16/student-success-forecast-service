@echo off
SET CONDA_PATH=%1

call "%CONDA_PATH%" activate student-predictions-env

Rscript on-site-script.R

call conda deactivate

echo Script execution completed.
