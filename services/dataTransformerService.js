const { 
    calculateDifficulty,
    normalizeSkewness
} = require('./dataCalculationService.js');

function predefineInAndOutTests(data) {
    return data.map(entry => {
        entry['is_in'] = !entry.test_score_ignored && entry.use_in_stats
        return entry
    })
}


function determineIfTestIsBeforeCutoff(data, beforeCutoffTestIdArray) {
    data.forEach((entry) => {
        entry['use_for_feature'] = beforeCutoffTestIdArray.includes(entry.test_id)
    })

    return data;
}


function calculateYearWeights(n) {
    if (!Number.isInteger(n) || n <= 0) {
        throw new Error('Input should be a positive integer');
    }

    if (n === 1) return [1];

    let result = [];
    const step = 1 / (n - 1);

    for (let i = 0; i < n; i++) {
        result.push(1 - step * i);
    }

    return result;
}


/**
 * 
 * @param {string[]} testGroups 
 * @param {Object.<number, string>} testGroupsLookUpTable 
 * @param {number} yearId
 * @param {Object[]} rawTestInstanceData 
 * @param {number} maxCourseScoreOverride
 * @returns {Object[]} Reduced test scores to one entry per student
 */
function formatTestGroupFeatures(testGroups, testGroupsLookUpTable, yearId, rawTestInstanceData, maxCourseScoreOverride = null) {
    let transformedData = [];
    let currentStudentId = null;
    let newEntry = null;

    let featureName = null;

    for (const studentTestScore of rawTestInstanceData) {
        if (currentStudentId !== studentTestScore.id_student) {
            currentStudentId = studentTestScore.id_student;

            if (newEntry) {
                processNewEntry(newEntry, testGroups, maxCourseScoreOverride);
                transformedData.push(newEntry);
            }

            newEntry = {
                id_student: studentTestScore.id_student,
                id_year: yearId,
                course_score: 0,
                course_max_score: 0,
                score: 0
            };

            for (const groupName of testGroups) {
                initializeNewEntryValues(newEntry, groupName);
            }
        }

        if (studentTestScore.use_for_feature) {
            featureName = testGroupsLookUpTable[studentTestScore.test_id] + '_score';
            newEntry[featureName] += studentTestScore.score !== null ? studentTestScore.score : 0;
        }
        newEntry['course_score'] += studentTestScore.score !== null ? studentTestScore.score : 0;

        if (studentTestScore.use_for_feature) {
            featureName = testGroupsLookUpTable[studentTestScore.test_id] + '_max_score';
            newEntry[featureName] += studentTestScore.max_score;
        }
        newEntry['course_max_score'] += studentTestScore.max_score; //this will be here for debugging purposes

    }

    // Handle the last entry
    if (newEntry) {
        processNewEntry(newEntry, testGroups, maxCourseScoreOverride);
        transformedData.push(newEntry);
    }


    // Calculate difficulty of groups
    for (const groupName of testGroups) {
        let featureName = groupName + '_score_achieved'
        let testGroupScores = getColumnValues(transformedData, featureName)
        featureName = groupName + '_difficulty'

        // IMPORTANT: Implement case when ALL students have the same score -> this throws an error for now (divide by 0)
        const skewness = calculateDifficulty(testGroupScores)
        const minSkewness = -3
        const maxSkewness = 3
        const normalizedSkewness = normalizeSkewness(skewness, minSkewness, maxSkewness); // so it clips values outside range
        transformedData = updateColumnValues(transformedData, featureName, normalizedSkewness);
    }


    return transformedData;
}
/**
 * @param {Object[]} entry
 * @param {string[]} testGroups
 * @param {number} maxScoreOverride
 * 
 * Sets the entry object score features values based on the 
 * informative features 'course_max_score', '${group}_score', '${group}_max_score'
 */
function processNewEntry(entry, testGroups, maxScoreOverride = null) {
    if (maxScoreOverride) {
        entry['course_max_score'] = maxScoreOverride;
    }

    for (const groupName of testGroups) {
        entry[groupName + '_score_achieved'] = entry[groupName + '_score'] / entry[groupName + '_max_score'];
        if (entry[groupName + '_score_achieved'] < 0) entry[groupName + '_score_achieved'] = 0;

        entry[groupName + '_of_overall_grade'] = entry[groupName + '_max_score'] / entry['course_max_score'];
    }

    entry['score'] = entry['course_score'] / entry['course_max_score']
}

function initializeNewEntryValues(entry, groupName) {
    entry[groupName + '_score'] = 0;
    entry[groupName + '_max_score'] = 0;
    // it is defined now so it is ordered in datatables later
    entry[groupName + '_score_achieved'] = null;
    entry[groupName + '_of_overall_grade'] = null;
    entry[groupName + '_difficulty'] = null;
}

function selectColumns(data, columnsToInclude) {
    return data.map((row) => {
        const newRow = {};
        columnsToInclude.forEach((column) => {
            if (row.hasOwnProperty(column)) {
                newRow[column] = row[column];
            }
        });
        return newRow;
    });
}

function addColumnToData(data, columnName, columnValue) {
    return data.map(entry => ({ ...entry, [columnName]: columnValue }));
}

function getColumnValues(data, columnName) {
    return data.map(entry => entry[columnName]);
}

function getUniqueColumnValues(data, columnName) {
    return [...new Set(data.map(entry => entry[columnName]))];
}

function updateColumnValues(data, columnName, columnValue) {
    return data.map(entry => ({ ...entry, [columnName]: columnValue }));
}

function getRowsWithColumnValue(data, columnName, columnValue) {
    return data.filter(entry => entry[columnName] === columnValue);
}

// this will be a label encoded column - 5 classes for 5 grades
// grade: 1, 2, 3, 4, 5 -> label: 1, 2, 3, 4, 5
// array of score to grade mapping is optional input
// default mapping is: [0, 50) -> 1, [50, 62.5) -> 2, [62.5, 75) -> 3, [75, 87.5) -> 4, [87.5, 100] -> 5
function addGradeToStudentData(data, scoreColumn, gradeColumn,
    scoreToGradeDividers = [50, 62.5, 75, 87.5]) {
    return data.map(entry => {
        const score = entry[scoreColumn];
        let grade = 5;
        for (let i = 0; i < scoreToGradeDividers.length; i++) {
            if (score < scoreToGradeDividers[i]) {
                grade = i + 1;
                break;
            }
        }
        return { ...entry, [gradeColumn]: grade };
    });
}

function addPassFlagToStudentData(data, scoreColumn, passColumn, passThreshold = 50) {
    return data.map(entry => {
        const score = entry[scoreColumn];
        const pass = score >= passThreshold ? 1 : 0;
        return { ...entry, [passColumn]: pass };
    });
}

module.exports = {
    formatTestGroupFeatures,
    determineIfTestIsBeforeCutoff,
    addColumnToData,
    getColumnValues,
    getUniqueColumnValues,
    updateColumnValues,
    getRowsWithColumnValue,
    selectColumns,
    predefineInAndOutTests,
    calculateYearWeights,
    addGradeToStudentData,
    addPassFlagToStudentData
}