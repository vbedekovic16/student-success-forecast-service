const fs = require('fs');

/**
 * Prints a formatted table to the console and optionally saves it to a text file.
 * @param {Array} data - The array of objects representing the table data.
 * @param {string} [outputFileName] - Optional parameter to specify the output file name.
 */
function printTxtTable(data, outputFileName) {
    // Get the headers from the first object in the array
    const headers = Object.keys(data[0]);

    // Calculate the maximum width for each column
    const columnWidths = {};
    headers.forEach(header => {
        const maxHeaderWidth = Math.max(...data.map(row => String(row[header]).length));
        columnWidths[header] = Math.max(maxHeaderWidth, header.length);
    });

    // Print headers
    let table = '   | ';
    headers.forEach(header => {
        table += `${header.padEnd(columnWidths[header])} | `;
    });
    table += '\n';

    // Print separator line
    table += '---|';
    headers.forEach(header => {
        table += '-'.repeat(columnWidths[header] + 1) + '-|';
    });
    table += '\n';

    // Print rows
    data.forEach((row, index) => {
        table += `${String(index + 1).padStart(3)}| `;
        headers.forEach(header => {
            table += `${String(row[header]).padEnd(columnWidths[header])} | `;
        });
        table += '\n';
    });

    //console.log(table);

    // Save to a text file if outputFileName is provided
    if (outputFileName) {
        fs.writeFileSync(outputFileName + '.txt', table);
    }
}

function writeColumnToTxt(data, columnName, filePath = 'output.txt') {
    const columnValues = data.map(entry => entry[columnName]);

    const content = columnValues.join('\n');

    fs.writeFile(filePath, content, 'utf8', (err) => {
        if (err) {
            console.error(err);
        } else {
            console.log(`Values from the "${columnName}" column have been written to ${filePath}`);
        }
    });
}

/**
 * Save the generated data to a csv file.
 * 
 * @param {object[]} data Array of objects that all have the same attributes.
 * @param {string} filename Don't provide extension.
 */
function saveToCSV(data, filename) {
    const csvStream = fs.createWriteStream(filename + '.csv')


    // Get the headers from the first object in the array
    const headers = Object.keys(data[0]);
    csvStream.write(headers.join(',') + '\n');

    data.forEach(row => {
        const csvRow = headers.map(header => row[header]).join(','); // join the values 
        csvStream.write(csvRow + '\n');
    })

    csvStream.end()
    
    console.log(`Data has been saved to ${filename}.csv`);
}

const readline = require('readline');

async function readFromCSV(filename) {
    const fileStream = fs.createReadStream(filename + '.csv');

    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    let headers;
    const data = [];

    for await (const line of rl) {
        const values = line.split(',');

        if (!headers) {
            headers = values;
        } else {
            const row = {};
            headers.forEach((header, i) => {
                row[header] = Number(values[i]);
            });
            data.push(row);
        }
    }

    console.log(`Data has been read from ${filename}.csv`);

    return data;
}

module.exports = { printTxtTable, writeColumnToTxt, saveToCSV, readFromCSV };