const db = require('../db').db;
const os = require('os');
const { v4: uuidv4 } = require('uuid');
var config = require('./../config/config');
var winston = require('winston');
const fs = require('fs');
const path = require('path');
const axios = require('axios');
const rimraf = require('rimraf');
const { 
    generateScorePredictionDataFromConfig,
    getDefinedGroupsBeforePredictionPoint,
    addStudentPerformanceForRelevantCoursesToData
} = require('./dataAggregatorService');
const codeExecutionStrategy = require('../code-execution-strategy');
const { saveToCSV, printTxtTable, readFromCSV } = require('./dataViewService');
const globals = require('../common/globals');

const STUDENT_FORECAST_FOLDER = path.join(__dirname, 'coderunner-files');
const JUDGE0_RUN_FILE_NAME = 'run';
const JUDGE0_R_SCRIPT_WRAPPER_FILE_NAME = 'judge0-script.R';
const ON_SITE_R_SCRIPT_WRAPPER_FILE_NAME = 'on-site-script.R';
const ON_SITE_RUN_FILE_NAME = os.platform() === 'win32' ? 'conda-script-windows.cmd' : 'conda-script-linux.sh';

const RUN_FILE_NAME = config.CODE_EXECUTION_STRATEGY === 'judge0' ? JUDGE0_RUN_FILE_NAME : ON_SITE_RUN_FILE_NAME;
const SCRIPT_WRAPPER_FILE_NAME = config.CODE_EXECUTION_STRATEGY === 'judge0' ? JUDGE0_R_SCRIPT_WRAPPER_FILE_NAME : ON_SITE_R_SCRIPT_WRAPPER_FILE_NAME;
//const MAIN_R_FILE_NAME = 'student-success-forecast-main-script.R';


const PASS_PREDICTION_TYPE = 'pass';
const GRADE_PREDICTION_TYPE = 'grade';

const TRAINING_SCRIPT = 'student-success-forecast-training-script.R';
const PREDICTION_SCRIPT = 'student-success-forecast-prediction-script.R';

// File system queries #############################################################################################
function saveCodeRunnerLogFile(courseId, academicYearId, predictionPoint, stage, logFile) {
    const logFileName = `${courseId}-${academicYearId}-${predictionPoint}-${stage}-terminal-output.log`;
    const logFilePath = path.join(globals.CODE_RUNNER_LOGS_FOLDER, logFileName);
    fs.writeFileSync(logFilePath, logFile);

    return logFileName;
}

function getSavedCodeRunnerLogFile(courseId, academicYearId, predictionPoint) {
    // TODO
}

function saveModelFile(courseId, academicYearId, predictionPoint, type, modelFile, fileExt) {
    const modelFileName = `${courseId}-${academicYearId}-${predictionPoint}-${type}-model.${fileExt}`;
    const modelFilePath = path.join(globals.MACHINE_LEARNING_MODELS_FOLDER, modelFileName);
    fs.writeFileSync(modelFilePath, modelFile);

    return modelFileName;
}

function getSavedModelFile(courseId, academicYearId, predictionPoint, type) {
    // TODO
}

// SQL queries #####################################################################################################
function buildGetAllRegisteredMethodsQuery() {
    return `
        SELECT id, prediction_type, name_method, description_method 
            FROM student_forecast.registered_method;
    `;
}

function buildGetAllPredictionTypeRegisteredMehodsQuery(prediction_type) {
    return `
        SELECT id, prediction_type, name_method, description_method 
            FROM student_forecast.registered_method
            WHERE prediction_type = '${prediction_type}';
    `;
}

function buildGetSelectRegisteredMethodsWithFileQuery(methodIds) {
    return `
        SELECT id, prediction_type, name_method, description_method, convert_from(file::bytea, 'UTF8') AS file_content
            FROM student_forecast.registered_method
            WHERE id IN (${methodIds.join(',')});
    `;
}

// #################################################################################################################

function copyTemplateFile(tempFolderPath, templateFileName) {
    const destinationPath = path.join(tempFolderPath, templateFileName);
    fs.copyFileSync(path.join(STUDENT_FORECAST_FOLDER, templateFileName), destinationPath);
    //console.log(path.join(STUDENT_FORECAST_FOLDER, templateFileName))
    //console.log(destinationPath)
}

function copyAllTemplateFiles(tempFolderPath, mainSourceFilePath) {
    copyTemplateFile(tempFolderPath, SCRIPT_WRAPPER_FILE_NAME);
    copyTemplateFile(tempFolderPath, RUN_FILE_NAME);
    copyTemplateFile(tempFolderPath, mainSourceFilePath);
}

async function populateMethodsFolderByType(methodsFolderPath, predictionType) {
    const allMethodsOfType = await db.query(buildGetAllPredictionTypeRegisteredMehodsQuery(predictionType));

    const methodIds = allMethodsOfType.map(method => method.id);
    const methods = await db.query(buildGetSelectRegisteredMethodsWithFileQuery(methodIds));

    // Write R files
    for (const method of methods) {
        fs.writeFileSync(path.join(methodsFolderPath, `${method.name_method}.R`), method.file_content);
    }
}

async function populateMethodsFolderByMethodIdArray(methodsFolderPath, methodIds) {
    const methods = await db.query(buildGetSelectRegisteredMethodsWithFileQuery(methodIds));

    // Write R files
    for (const method of methods) {
        fs.writeFileSync(path.join(methodsFolderPath, `${method.name_method}.R`), method.file_content);
    }
}

async function generateStudentDataCSVFile(config, predictionPoint, tempFolderPath) {

    let studentData = await generateScorePredictionDataFromConfig(config, predictionPoint - 1);
    // -1 because the function takes the index as input

    const relevantCoursesData = config.selectedCourseCorrelationAdditionalFeaturesCourses
    studentData = 
        await addStudentPerformanceForRelevantCoursesToData(
            config.courseId,
            studentData,
            relevantCoursesData
        );

    const csvName = `${config.courseId}-${config.yearId}-p${predictionPoint}-student-data`;
    const filePath = path.join(tempFolderPath, csvName);
    saveToCSV(studentData, filePath);
    //printTxtTable(studentData, filePath);
    
    return csvName + '.csv';
}

async function generateTrainingParametersJSON(config, predictionPoint, tempFolderPath, studentDataCSVFile, mainSourceFilePath, modelsInfo = null) {
    const groupsBeforePredictionPoint = getDefinedGroupsBeforePredictionPoint(config, predictionPoint - 1);
    const parameters = {
        main_source_file_path: mainSourceFilePath,
        number_of_folds: 10,
        course_id: config.courseId,
        prediction_point: predictionPoint,
        groups: groupsBeforePredictionPoint,
        additional_features: config.selectedCourseCorrelationAdditionalFeaturesCourses.map(course => course.course_acronym + globals.CORRELATION_COURSE_PERFORMANCE_FEATURE_SUFFIX),
        suffixes: config.selectedPrimaryGroupFeaturesSuffixes,
        data_filename: studentDataCSVFile,
        pass_methods_folder: 'pass-methods',
        grade_methods_folder: 'grade-methods',
    };

    if (modelsInfo) {
        parameters.pass_best_method = modelsInfo.passMethodName;
        parameters.pass_model_filename = modelsInfo.passModelFileNameWithExt;
        parameters.grade_best_method = modelsInfo.gradeMethodName;
        parameters.grade_model_filename = modelsInfo.gradeModelFileNameWithExt;
    }

    const parametersJSON = JSON.stringify(parameters);
    fs.writeFileSync(path.join(tempFolderPath, 'parameters.json'), parametersJSON);
}


async function populateFolderWithNeededFilesForCodeRunnerModelTraining(config, predictionPoint, tempFolderPath) {
    const passMethodsFolderPath = path.join(tempFolderPath, 'pass-methods');
    fs.mkdirSync(passMethodsFolderPath);
    const gradeMethodsFolderPath = path.join(tempFolderPath, 'grade-methods');
    fs.mkdirSync(gradeMethodsFolderPath);

    await populateMethodsFolderByMethodIdArray(passMethodsFolderPath, config.selectedPassFailMethods);
    await populateMethodsFolderByMethodIdArray(gradeMethodsFolderPath, config.selectedGradeMethods);
    const studentDataCSVFileName = await generateStudentDataCSVFile(config, predictionPoint, tempFolderPath);
    await generateTrainingParametersJSON(config, predictionPoint, tempFolderPath, studentDataCSVFileName, TRAINING_SCRIPT);
    copyAllTemplateFiles(tempFolderPath, TRAINING_SCRIPT);
}

async function populateFolderWithNeededFilesForCodeRunnerModelPrediction(
    config,
    modelsInfo, 
    tempFolderPath
) {
    const predictionPointOverride = 1;

    const passMethodsFolderPath = path.join(tempFolderPath, 'pass-methods');
    fs.mkdirSync(passMethodsFolderPath);
    const gradeMethodsFolderPath = path.join(tempFolderPath, 'grade-methods');
    fs.mkdirSync(gradeMethodsFolderPath);
    await populateMethodsFolderByMethodIdArray(passMethodsFolderPath, [modelsInfo.passMethodId]);
    await populateMethodsFolderByMethodIdArray(gradeMethodsFolderPath, [modelsInfo.gradeMethodId]);

    const passModelDestinationPath = path.join(tempFolderPath, modelsInfo.passModelFileNameWithExt);
    const savedPassModelLocationPath = path.join(globals.MACHINE_LEARNING_MODELS_FOLDER, modelsInfo.passModelFileNameWithExt);
    fs.copyFileSync(savedPassModelLocationPath, passModelDestinationPath);

    const gradeModelDestinationPath = path.join(tempFolderPath, modelsInfo.gradeModelFileNameWithExt);
    const savedGradeModelLocationPath = path.join(globals.MACHINE_LEARNING_MODELS_FOLDER, modelsInfo.gradeModelFileNameWithExt);
    fs.copyFileSync(savedGradeModelLocationPath, gradeModelDestinationPath);

    const studentDataCSVFileName = await generateStudentDataCSVFile(config, predictionPointOverride, tempFolderPath);
    await generateTrainingParametersJSON(config, predictionPointOverride, tempFolderPath, studentDataCSVFileName, PREDICTION_SCRIPT, modelsInfo);
    copyAllTemplateFiles(tempFolderPath, PREDICTION_SCRIPT);
}

async function postCodeSubmission(base64EncodedZip) {
    // Send POST request
    const postResponse = await axios.post(`${config.JUDGE0_API_URL}/submissions/`, {
        additional_files: base64EncodedZip,
        language_id: 89
    }, {
        params: {
            base64_encoded: false,
            wait: false,
            memory_limit: 1000000,
            cpu_time_limit: 43200,
            wall_time_limit: 86400
        }
    });

    const token = postResponse.data.token;

    let statusId = 1;
    let getResponse;

    // Poll the server until the status changes
    while (statusId === 1 || statusId === 2) {
        winston.info(`Polling Judge0 for submission ${token}...`);
        await new Promise(resolve => setTimeout(resolve, POLLING_TIME_MS)); // wait for 5 seconds

        getResponse = await axios.get(`${config.JUDGE0_API_URL}/submissions/${token}?base64_encoded=false&fields=stdout,status`);
        statusId = getResponse.data.status.id;
    }
    return getResponse;
}


function deleteTempFolders(tempFolderPath){
    // Delete the directory, input zip and output zip
    rimraf.sync(tempFolderPath);
    if (fs.existsSync(`${tempFolderPath}.zip`)) rimraf.sync(`${tempFolderPath}.zip`);
    if (fs.existsSync(`${tempFolderPath}.tar.gz`)) rimraf.sync(`${tempFolderPath}.tar.gz`);
}


async function executeModelTraining(config, predictionPoint) {
    try {
        const tempFolderPath = path.join(STUDENT_FORECAST_FOLDER, `student-forecast-${uuidv4()}`);
        fs.mkdirSync(tempFolderPath);
        await populateFolderWithNeededFilesForCodeRunnerModelTraining(config, predictionPoint, tempFolderPath);

        await codeExecutionStrategy.executeCode(tempFolderPath);

        const logFile = fs.readFileSync(path.join(tempFolderPath, 'logs', 'log.txt'));
        const passMetricsFile = fs.readFileSync(path.join(tempFolderPath, 'files', 'pass-metrics.json'));
        const gradeMetricsFile = fs.readFileSync(path.join(tempFolderPath, 'files', 'grade-metrics.json'));
        const ensembleMetricsFile = fs.readFileSync(path.join(tempFolderPath, 'files', 'ensemble-metrics.json'));
        const modelSaveInfoFile = fs.readFileSync(path.join(tempFolderPath, 'files', 'saved-model-file-paths.json'));
        const dataInfoFile = fs.readFileSync(path.join(tempFolderPath, 'files', 'data-info.json'));

        const modelSaveInfo = await JSON.parse(modelSaveInfoFile);
        const gradeModelFile = fs.readFileSync(path.join(tempFolderPath, 'files', modelSaveInfo.grade_model_file));
        const passModelFile = fs.readFileSync(path.join(tempFolderPath, 'files', modelSaveInfo.pass_model_file));

        const passModelExt = modelSaveInfo.pass_model_file.split('.').pop();
        const gradeModelExt = modelSaveInfo.grade_model_file.split('.').pop();
        const passModelNameWithExt = saveModelFile(config.courseId, config.yearId, predictionPoint, PASS_PREDICTION_TYPE, passModelFile, passModelExt);
        const gradeModelNameWithExt = saveModelFile(config.courseId, config.yearId, predictionPoint, GRADE_PREDICTION_TYPE, gradeModelFile, gradeModelExt);

        const modelData = {
            passModelMethodName: modelSaveInfo.pass_method,
            passModelNameWithExt: passModelNameWithExt,

            gradeModelMethodName: modelSaveInfo.grade_method,
            gradeModelNameWithExt: gradeModelNameWithExt,
        }

        
        // Print the contents of the log.txt file
        //console.log(logFile.toString());
        const logNameWithExt = saveCodeRunnerLogFile(config.courseId, config.yearId, predictionPoint, 'training', logFile);
        const codeLogData = {
            codeLog: logNameWithExt
        }


        //fs.writeFileSync(path.join(STUDENT_FORECAST_FOLDER, 'code-execution.log'), logFile);
        //console.log(passMetricsFile.toString());
        //console.log(gradeMetricsFile.toString());
        //console.log(ensembleMetricsFile.toString());
        //console.log(modelSaveInfoFile.toString());
        //console.log(dataInfoFile.toString());

        deleteTempFolders(tempFolderPath);

        return [
            await JSON.parse(passMetricsFile),
            await JSON.parse(gradeMetricsFile),
            await JSON.parse(ensembleMetricsFile),
            await JSON.parse(dataInfoFile),
            modelData,
            codeLogData
        ]
    }
    catch (error) {
        console.error('Error while preparing or executing code:', error);
        throw error;
    }
}

async function executeModelPrediction(config, predictionPoint, modelsInfo) {
    try {
        const tempFolderPath = path.join(STUDENT_FORECAST_FOLDER, `student-forecast-${uuidv4()}`);
        fs.mkdirSync(tempFolderPath);
        await populateFolderWithNeededFilesForCodeRunnerModelPrediction(config, modelsInfo, tempFolderPath);

        await codeExecutionStrategy.executeCode(tempFolderPath);

        const logFile = fs.readFileSync(path.join(tempFolderPath, 'logs', 'log.txt'));
        const logNameWithExt = saveCodeRunnerLogFile(config.courseId, config.yearId, predictionPoint, 'prediction', logFile);
        const codeLogData = {
            codeLog: logNameWithExt
        }

        const predictionInfoJSON = fs.readFileSync(path.join(tempFolderPath, 'files', 'prediction-info.json'));
        const predictionInfo = await JSON.parse(predictionInfoJSON);
        console.log(predictionInfo);

        const predictionResultsFileNameCSV = predictionInfo.results_file_name_csv;
        const predictionResults = await readFromCSV(path.join(tempFolderPath, 'files', predictionResultsFileNameCSV.split('.').shift()));
        
        deleteTempFolders(tempFolderPath);

        return [
            predictionResults,
            codeLogData
        ]

    } catch (error) {
        console.error('Error while preparing or executing code:', error);
        throw error;
    }
}

async function executeMethodValidation(methodString, type) {
    try {
        const tempFolderPath = path.join(STUDENT_FORECAST_FOLDER, `method-validation-${uuidv4()}`);
        fs.mkdirSync(tempFolderPath);
        const filePath = path.join(tempFolderPath, 'new-method.R');
        fs.writeFileSync(filePath, methodString, 'utf-8');
        copyTemplateFile(tempFolderPath, SCRIPT_WRAPPER_FILE_NAME);
        copyTemplateFile(tempFolderPath, RUN_FILE_NAME);
        copyTemplateFile(tempFolderPath, `posted-${type}-method-validation-script.R`);
        const parameters = {
            main_source_file_path: `posted-${type}-method-validation-script.R`,
        };
        const parametersJSON = JSON.stringify(parameters);
        fs.writeFileSync(path.join(tempFolderPath, 'parameters.json'), parametersJSON);

        await codeExecutionStrategy.executeCode(tempFolderPath);

        const errorReportJSON = fs.readFileSync(path.join(tempFolderPath, 'files', 'error-report.json'));
        const errorReport = await JSON.parse(errorReportJSON);

        deleteTempFolders(tempFolderPath);

        return errorReport;
    
    } catch (error) {
        console.error('Error while preparing or executing code:', error);
        throw error;
    }
}

module.exports = {
    executeModelTraining,
    executeModelPrediction,
    executeMethodValidation
};


