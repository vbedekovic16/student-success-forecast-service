const db = require("../db").db;

/*id course 2017
academic years: grupa        2021,      2022
test ids:       projAuth     13465      13938
                MI           13539      14039
                projSPA      13594      14069
                ZI           13645      14106
*/

/*jump a few steps forward 
 
*/


async function getStudentScores(id_course, id_academic_year, id_test_array) {
    if (!Number.isInteger(id_course) || !Number.isInteger(id_academic_year) || !Array.isArray(id_test_array)) {
        throw new TypeError('Invalid parameters. id_course and id_academic_year must be integers, and id_test_array must be an array of integers.');
    }

    const studentScores = await db.query(`
            WITH AllPossibleCombinations AS (
            SELECT
            student.id AS id_student,
            test.id AS test_id,
            test.title,
            test.max_score
            FROM
            student
            LEFT JOIN student_course ON student.id = student_course.id_student
            CROSS JOIN test
            WHERE
            student_course.id_course = $1   
            AND student_course.id_academic_year = $2  
            AND student.id_app_user IS NULL
            AND test.id_course = $1           
            AND test.id_academic_year = $2 
            AND test.id = ANY($3)
        )
        , AllStudentsWithDummyTestInstances AS (
            SELECT
            apc.id_student,
            apc.test_id,
            apc.title,
            apc.max_score,
            NULL::int AS id_test_instance,
            NULL::int AS score,
            NULL::timestamp AS ts_submitted
            FROM
            AllPossibleCombinations apc
        )
        , RankedTestInstances AS (
            SELECT
            ast.id_student AS ranked_id_student,
            ast.test_id,
            ast.title,
            ast.max_score,
            test_instance.*,
            ROW_NUMBER() OVER (PARTITION BY ast.id_student, ast.test_id ORDER BY test_instance.ts_submitted DESC) AS rnk
            FROM
            AllStudentsWithDummyTestInstances ast
            LEFT JOIN test_instance ON ast.id_student = test_instance.id_student
                                    AND ast.test_id = test_instance.id_test
        )
        SELECT ranked_id_student AS id_student, test_id, title, score, max_score
        FROM RankedTestInstances
        WHERE rnk = 1
        ORDER BY ranked_id_student;
        `, [id_course, id_academic_year, id_test_array])


    const processedRows = studentScores.map(row => ({
        id_student: row.id_student,
        test_id: row.test_id,
        title: row.title,
        score: row.score !== null ? Number(row.score) : null,
        max_score: Number(row.max_score)
    }));
    return processedRows;
}

async function getTestsForCourseYear(course, year, minNumOfStudentsThatTookTest = 0) {
    const tests = await db.query(`
        SELECT test.id, test.title, test.ts_available_from, test.max_score,
            COUNT(test_instance.id) AS num_of_students_that_took_test
            FROM test
            INNER JOIN 
                test_instance ON test.id = test_instance.id_test AND test_instance.ts_submitted IS NOT NULL
            WHERE 
                test.id_course = $1 			
                AND test.id_academic_year = $2
                AND test.max_runs = 1 
                AND test.test_score_ignored = FALSE 
                AND test.use_in_stats = TRUE 
            GROUP BY 
                test.id
            HAVING 
                COUNT(test_instance.id) >= $3
            ORDER BY
                ts_available_from
        `, [course, year, minNumOfStudentsThatTookTest])

    return tests;
}

async function getAcademicYearsEnroledStudedntsGraduatedFromRelevantCourse(course, currentCourseYear, relevantCourse) {
    const academicYears = await db.query(`
        SELECT DISTINCT sc.id_academic_year
            FROM student_course sc
            JOIN (
                SELECT id_student, MAX(id_academic_year) AS max_year
                FROM student_course
                WHERE id_course = $3
                GROUP BY id_student
            ) max_years ON sc.id_student = max_years.id_student AND sc.id_academic_year = max_years.max_year
            JOIN student ON sc.id_student = student.id
            WHERE 
                sc.id_course = $3
                AND id_app_user IS NULL
                AND sc.id_student IN (
                    SELECT id_student
                    FROM student_course
                    WHERE id_course = $1
                        AND id_academic_year = $2
                )
            ORDER BY sc.id_academic_year;
        `, [course, currentCourseYear, relevantCourse])
    return academicYears;
}

// TEST_SCORE_ACHIEVED × MAX_TEST_SCORE × NUM_OF_STUDENTS_THAT_TOOK_TEST
async function determineStudentPerformanceScoreForCourseYear(course, year, minNumOfStudentsThatTookTest) {

}


async function getEnroledStudentsFromCourseAndYear() {
    try {
        const client = await db.connect();
        const result = await client.query('SELECT id, type_name FROM test_type');

        const types = result.rows.map((r) => r.type_name)

        console.log('Test Types:');
        console.log(types);

        client.release();
    } catch (error) {
        console.error('Error getting test types:', error);
    }
}

async function getStudentCourseData() {
    try {
        const client = await db.connect();
        const result = await client.query('SELECT id, type_name FROM test_type');

        const types = result.rows.map((r) => r.type_name)

        console.log('Test Types:');
        console.log(types);

        client.release();
    } catch (error) {
        console.error('Error getting test types:', error);
    }
}



module.exports = {
    getStudentScores,
    getTestsForCourseYear,
    getAcademicYearsEnroledStudedntsGraduatedFromRelevantCourse
};