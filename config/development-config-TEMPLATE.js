module.exports = {
  PG_CONNECTION_STRING: 'pg://<user>:<password>@127.0.0.1:5432/<dbname>',
  db: 'edgar db',
  JUDGE0_API_URL: 'http://<host>:<port>',
  POLLING_TIME_MS: 5000,
  CONDA_BAT_PATH_WINDOWS: 'C:/Users/<user>/anaconda3/Library/bin/conda.bat',
  CONDA_BAT_PATH_LINUX: '/home/<user>/anaconda3/bin/conda',
  CODE_EXECUTION_STRATEGY: 'judge0', // judge0 or on-site
};
