var config = {
  development: require('./development-config.js'),
};

try {
  config.production = require('./production-config.js');
} catch (error) {
  console.warn('No production configuration file found (production-config.js). Create it from the development-config-TEMPLATE.js file.');
}

module.exports = config[(process.env.NODE_ENV || 'development').trim()];
