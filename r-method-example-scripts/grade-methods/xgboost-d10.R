library(xgboost)

train_model <- function(train_data, train_features, train_target, class_weights) {
    # Map weights for each entry in the training set
    entry_weights <- sapply(train_target, function(class_name) class_weights[[class_name]])

    # Convert the target to numeric
    train_target <- as.numeric(levels(train_target))[train_target] - 1
    
    # Convert the data to matrix format
    train_matrix <- xgb.DMatrix(data = as.matrix(train_features), label = train_target, weight = entry_weights)

    # Define the parameters for the xgboost model
    params <- list(
        objective = "multi:softprob",
        num_class = length(unique(train_target)),
        max_depth = 10,
        eta = 0.01
    )

    # Train the xgboost model
    xgb_model <- xgb.train(params = params, data = train_matrix, nrounds = 100)

    return(xgb_model)
}

get_prob_predictions <- function(xgb_model, test_features) {
    test_matrix <- xgb.DMatrix(data = as.matrix(test_features))
    prob_predictions_vector <- predict(xgb_model, newdata = test_matrix)
    prob_predictions <- matrix(prob_predictions_vector, byrow = TRUE, ncol = 5)   # because we have 5 grade classes
    colnames(prob_predictions) <- c("1", "2", "3", "4", "5")
    return(prob_predictions)
}

get_predictions <- function(xgb_model, test_features) {
    prob_predictions <- get_prob_predictions(xgb_model, test_features)
    predictions <- apply(prob_predictions, 1, function(x) names(which.max(x)))
    return(predictions)
}

save_model <- function(xgb_model, file_path) {
    file_path_with_ext <- paste(file_path, ".xgb", sep = "")
    xgb.save(xgb_model, file_path_with_ext)
    return(file_path_with_ext)
}

load_model <- function(file_path_with_ext) {
    xgb_model <- xgb.load(file_path_with_ext)
    return(xgb_model)
}