library(dplyr)
library(keras)
library(caret)

train_model <- function(train_data, train_features, train_target, class_weigts) {
    

    num_of_hidden_nodes <- nrow(train_data)

    dmy <- dummyVars(" ~ .", data = train_data, lvls = c("1", "2", "3", "4", "5"))
    train_data <- data.frame(predict(dmy, newdata = train_data))
    train_target <- train_data %>% 
        dplyr::select(grade.1, grade.2, grade.3, grade.4, grade.5)

    mlp_model <- keras_model_sequential() %>%
        layer_dense(
            units = num_of_hidden_nodes,
            kernel_initializer = "HeNormal",
            input_shape = ncol(train_features)
        ) %>%
        layer_activation_leaky_relu() %>%
        layer_dense(units = 5, activation = "softmax") %>%
        compile(
            optimizer = "rmsprop",
            loss = "categorical_crossentropy",
            metrics = c("categorical_accuracy")
        )

    mlp_model %>% fit(
        x = as.matrix(train_features),
        y = as.matrix(train_target),
        epochs = 10,
        batch_size = 32,
        validation_split = .2,
        verbose = FALSE,
        callbacks = list(
            callback_early_stopping(restore_best_weights = TRUE, patience = 10)
        )
    )

    return(mlp_model)
}

validate_model <- function(mlp_model, test_features, test_target) {
    predictions <- predict(mlp_model, as.matrix(test_features))
    predictions <- max.col(predictions) # Convert probabilities to class labels
    # this works because the index of the column matches the class (grade) label

    accuracy <- sum(predictions == test_target) / length(test_target)

    return(accuracy)
}

predict_with_model <- function(mlp_model, features, data) {
    data_features <- data[features]
    data_target <- data$grade
    student_ids <- data$id_student

    predictions_probs <- predict(mlp_model, as.matrix(data_features))

    predictions <- max.col(predictions_probs)

    results <- data.frame(student_id = student_ids, target = data_target, prediction = predictions)

    return(results)
}

save_model <- function(mlp_model, file_path) {
    save_model_hdf5(mlp_model, paste(file_path, ".keras", sep = ""))
}

load_model <- function(file_path) {
    mlp_model <- load_model_hdf5(paste(file_path, ".keras", sep = ""))
    return(mlp_model)
}