library(dplyr)
library(torch)
library(brulee)

train_model <- function(train_data, train_features, train_target) {
    num_of_hidden_nodes <- ceiling(nrow(train_data) / (2 * (ncol(train_features) + 1)))

    attempt_train_model <- function() {
        brulee_mlp(
            train_features,
            train_target,
            epochs = 100,
            hidden_units = num_of_hidden_nodes,
            activation = "leaky_relu",
            penalty = 0.001,
            mixture = 0,
            dropout = 0,
            validation = 0.1,
            optimizer = "LBFGS",
            learn_rate = 0.01,
            rate_schedule = "none",
            momentum = 0,
            batch_size = NULL,
            class_weights = NULL,
            stop_iter = 10,
            verbose = FALSE
        )
    }

    # Attempt to train the model, and retry if an error occurs...
    # This is to prevent the "best_epoch is not an integer" bug (happens rarely)...
    mlp_model <- tryCatch(
        {
            attempt_train_model()
        },
        error = function(e) {
            if (grepl("'best_epoch' should be an integer", conditionMessage(e))) {
                cat("Error occurred:", conditionMessage(e), "\n")
                cat("Retrying...\n")
                attempt_train_model()  # Retry if the specific error occurs
            } else {
                # If the error is different, rethrow it
                stop(e)
            }
        }
    )

    return(mlp_model)
}

get_prob_predictions <- function(mlp_model, data_features) {
    prob_predictions <- predict(mlp_model, data_features, type = "prob")
    colnames(prob_predictions) <- c("0", "1")
    return(as.matrix(prob_predictions))
}

get_predictions <- function(mlp_model, data_features) {
    predictions <- predict(mlp_model, data_features)
    return(as.matrix(predictions))
}

save_model <- function(mlp_model, file_path) {
    file_path_with_ext <- paste(file_path, ".rds", sep = "")
    saveRDS(mlp_model, file = file_path_with_ext)
    return(file_path_with_ext)
}

load_model <- function(file_path_with_ext) {
    mlp_model <- readRDS(file_path_with_ext)
    return(mlp_model)
}
