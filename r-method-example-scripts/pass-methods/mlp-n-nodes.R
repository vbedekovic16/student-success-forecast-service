library(dplyr)
library(keras)

train_model <- function(train_data, train_features, train_target) {
    num_of_hidden_nodes <- nrow(train_data)

    train_target <- as.numeric(train_data$pass) - 1

    mlp_model <- keras_model_sequential() %>%
        layer_dense(
            units = num_of_hidden_nodes,
            kernel_initializer = "HeNormal",
            input_shape = ncol(train_features)
        ) %>%
        layer_activation_leaky_relu() %>%
        layer_dense(units = 1, activation = "sigmoid") %>%
        compile(
            optimizer = "rmsprop",
            loss = "binary_crossentropy",
            metrics = c("accuracy")
        )

    mlp_model %>% fit(
        x = as.matrix(train_features),
        y = train_target,
        epochs = 100,
        batch_size = 32,
        validation_split = .2,
        verbose = FALSE
    )

    return(mlp_model)
}

validate_model <- function(mlp_model, test_features, test_target) {
    predictions_probs <- predict(mlp_model, as.matrix(test_features))
    # For binary classification, we don't need to add 1 to the predictions
    # predictions <- apply(predictions, 1, which.max) + 1
    predictions <- ifelse(predictions_probs > 0.5, 1, 0)  # Convert probabilities to class labels


    accuracy <- sum(predictions == test_target) / length(test_target)

    return(accuracy)
}

predict_with_model <- function(mlp_model, features, data) {
    data_features <- data[features]
    data_target <- data$pass 
    student_ids <- data$id_student

    predictions_probs <- predict(mlp_model, as.matrix(data_features))

    predictions <- ifelse(predictions_probs > 0.5, 1, 0)

    results <- data.frame(student_id = student_ids, target = data_target, prediction = predictions)

    return(results)
}

save_model <- function(mlp_model, file_path) {
    save_model_hdf5(mlp_model, paste(file_path, ".keras", sep = ""))
}

load_model <- function(file_path) {
    mlp_model <- load_model_hdf5(paste(file_path, ".keras", sep = ""))
    return(mlp_model)
}