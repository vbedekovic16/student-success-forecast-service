library(randomForest)

train_model <- function(train_data, train_features, train_target) {
    rf_model <- randomForest(
        formula = pass ~ .,
        data    = train_data,
        ntree   = 2000
    )

    return(rf_model)
}

get_prob_predictions <- function(rf_model, data_features) {
    prob_predictions <- predict(rf_model, newdata = data_features, type = "prob")
    return(prob_predictions)
}

get_predictions <- function(rf_model, data_features) {
    predictions <- predict(rf_model, newdata = data_features)
    return(predictions)
}

save_model <- function(rf_model, file_path) {
    file_path_with_ext <- paste(file_path, ".rds", sep = "")
    saveRDS(rf_model, file = file_path_with_ext)
    return(file_path_with_ext)
}

load_model <- function(file_path_with_ext) {
    rf_model <- readRDS(file_path_with_ext)
    return(rf_model)
}