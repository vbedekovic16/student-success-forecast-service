library(dplyr)
library(e1071)


train_model <- function(train_data, train_features, train_target) {
    # The optimal K value usually found is the square root of N,
    # where N is the total number of samples
    optimal_k <- round(sqrt(nrow(train_data)))
    cat("Optimal k is:", optimal_k, "\n")
    # Train a KNN regression model
    knn_model <- gknn(
        formula = pass ~ .,
        data    = train_data,
        k = optimal_k  # Number of neighbors
    )

    return(knn_model)
}

get_prob_predictions <- function(knn_model, data_features) {
    prob_predictions <- predict(knn_model, as.matrix(data_features), type = "prob")
    return(prob_predictions)
}

get_predictions <- function(knn_model, data_features) {
    predictions <- predict(knn_model, as.matrix(data_features))
    return(predictions)
}

save_model <- function(knn_model, file_path) {
    file_path_with_ext <- paste(file_path, ".rds", sep = "")
    saveRDS(knn_model, file = file_path_with_ext)
    return(file_path_with_ext)
}

load_model <- function(file_path_with_ext) {
    knn_model <- readRDS(file_path_with_ext)
    return(knn_model)
}