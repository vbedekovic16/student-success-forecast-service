library(dplyr)
library(e1071)

train_model <- function(train_data, train_features, train_target) {
    # Train an SVM regression model
    svm_model <- svm(
        formula = pass ~ .,
        data = train_data,
        kernel = "linear",
        probability = TRUE
    )

    return(svm_model)
}

get_prob_predictions <- function(svm_model, data_features) {
    prob_predictions <- predict(svm_model, newdata = data_features, probability=TRUE)
    return(attr(prob_predictions, "probabilities"))
}

get_predictions <- function(svm_model, data_features) {
    predictions <- predict(svm_model, newdata = data_features)
    return(predictions)
}

save_model <- function(svm_model, file_path) {
    file_path_with_ext <- paste(file_path, ".rds", sep = "")
    saveRDS(svm_model, file = file_path_with_ext)
    return(file_path_with_ext)
}

load_model <- function(file_path_with_ext) {
    svm_model <- readRDS(file_path_with_ext)
    return(svm_model)
}