library(xgboost)

train_model <- function(train_data, train_features, train_target) {
    # Convert the target to numeric
    train_target <- as.numeric(levels(train_target))[train_target]
    
    # Convert the data to matrix format
    train_matrix <- xgb.DMatrix(data = as.matrix(train_features), label = train_target)

    # Define the parameters for the xgboost model
    params <- list(
        objective = "binary:logistic",
        max_depth = 8,
        eta = 0.01
    )

    # Train the xgboost model
    xgb_model <- xgb.train(params = params, data = train_matrix, nrounds = 100)

    return(xgb_model)
}

get_prob_predictions <- function(xgb_model, data_features) {
    data_matrix <- xgb.DMatrix(data = as.matrix(data_features))
    prob_predictions <- predict(xgb_model, newdata = data_matrix)
    result_matrix <- cbind(1 - prob_predictions, prob_predictions)
    colnames(result_matrix) <- c("0", "1")
    return(result_matrix)
}

get_predictions <- function(xgb_model, data_features) {
    data_matrix <- xgb.DMatrix(data = as.matrix(data_features))
    prob_predictions <- predict(xgb_model, newdata = data_matrix)
    predictions <- ifelse(prob_predictions > 0.5, 1, 0)
    return(predictions)
}

save_model <- function(xgb_model, file_path) {
    file_path_with_ext <- paste(file_path, ".xgb", sep = "")
    xgb.save(xgb_model, file_path_with_ext)
    return(file_path_with_ext)
}

load_model <- function(file_path_with_ext) {
    xgb_model <- xgb.load(file_path_with_ext)
    return(xgb_model)
}