const globals = {
  SYMBOL_SPACE: '·',
  SYMBOL_ENTER: '↵',
  SYMBOL_TAB: '⭾',
  SYMBOL_NULL: '∅',
  SYMBOL_NON_PRINTABLE: '☐',
  WARNING_MESSAGE: null,
  APP_VERSION: '0.0.0',
  ALLOWED_ANONYMOUS_LOG_EVENTS: ['Lost focus', 'Exam submitted.'],

  MACHINE_LEARNING_MODELS_FOLDER: 'machine-learning-models',
  CODE_RUNNER_LOGS_FOLDER: 'code-runner-logs',

  CORRELATION_COURSE_PERFORMANCE_FEATURE_SUFFIX: '_performance',
  MIN_CORRELATION_THRESHOLD: 0.3,
  INFO_FEATURE_YEAR_ID: 'id_year',
  SCORE_FEATURE: 'score',
};

module.exports = globals;
