const express = require('express');
const cors = require('cors');
var winston = require('winston');
var osInfo = require('./common/osInfo');
var http = require('http');
var path = require('path');
var os = require('os');
var fs = require('fs');
const db = require('./db').db;
const { format } = require('winston');
const { errors } = format;
const { serverRestartTrainingJobsFetching } = require('./services/schedulerService');

let winstonFormat;
let level;
if (process.env.NODE_ENV === 'production') {
    level = 'info';
    winstonFormat = winston.format.printf(({ level, message, timestamp, stack }) => {
        if (stack) {
            return `${timestamp} ${level}: ${message} - ${stack}`;
        } else {
            return `${timestamp} ${level}: ${message}`;
        }
    });
} else {
    level = 'debug';
    winstonFormat = winston.format.printf(({ level, message, timestamp, stack }) => {
        if (stack) {
            return `${timestamp} ${level}: ${message} - ${stack}`;
        } else {
            return `${timestamp} ${level}: ${message}`;
        }
    });
}
if (!!process.env.LOG_LEVEL) {
    level = process.env.LOG_LEVEL;
}
winston.configure({
    //u formatu je dodan timestamp, i stavljen je json radi strukture
    format: winston.format.combine(
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        errors({ stack: true }),
        winstonFormat
    ),
    defaultMeta: {
        hostname: osInfo.getHostName(),
        script: osInfo.getScriptName(),
        appInstance: osInfo.appInstance(),
    },
    transports: [new winston.transports.Console()],
});
winston.level = level;

var port = parseInt(process.env.PORT || 4242);
port += process.env.NODE_APP_INSTANCE ? parseInt(process.env.NODE_APP_INSTANCE) : 0;
global.hostname = os.hostname();
global.appInstance = process.env.NODE_APP_INSTANCE ? parseInt(process.env.NODE_APP_INSTANCE) : 0;
global.appRoot = path.resolve(__dirname);

winston.info('global.appInstance ' + global.appInstance);
winston.info('global.appRoot ' + global.appRoot);

var globals = require('./common/globals');

let dir = `${global.appRoot}/${globals.MACHINE_LEARNING_MODELS_FOLDER}/`;
if (!fs.existsSync(dir)) {
  winston.info('Creating machine learning model folder: ' + dir);
  fs.mkdirSync(dir);
}
dir = `${global.appRoot}/${globals.CODE_RUNNER_LOGS_FOLDER}/`;
if (!fs.existsSync(dir)) {
  winston.info('Creating code runner logs folder: ' + dir);
  fs.mkdirSync(dir);
}

var config = require('./config/config');

const app = express();
app.use(cors());
app.use(express.json());

app.get('/status', (req, res) => {
    res.send('I am listening...');
});

const api = require('./api');
app.use('/api', api);


/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

var bindto = process.env.NODE_BIND || 'localhost';

server.listen(port, bindto, function () {
    winston.info(`--> listening on (bindto, port) = (${bindto}, ${server.address().port})`);
});

server.on('error', onError);
server.on('listening', onListening);

process.on('uncaughtException', function (err) {
    winston.error(err);
    winston.error('>>>>>>>>>>>>>>>>>>>>>>>> ------------ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    winston.error('>>>>>>>>>>>>>>>>>>>>>>>> LET IT DIE!! <<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    winston.error('>>>>>>>>>>>>>>>>>>>>>>>> ------------ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
    process.exit(1);
});

// done.

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    winston.error('>>>>>>>>>>>>>>>>>>>>>>>>>>> onError', error);
    winston.error(error);

    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    winston.info('********************************************');
    var addr = server.address();
    winston.info(`Server address is: ${JSON.stringify(addr)}`);
    var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    winston.info('Running the magnificent server on ' + bind);
    winston.info(`Using database: ${config.db}.`);
    winston.info(`Log level is ${winston.level}.`);
    winston.info('ENV is: ' + process.env.NODE_ENV);
    winston.info('Node version is: ' + process.versions.node);
    winston.info('********************************************');
    let summary =
        'Started Student Success Forecast service on host ' +
        os.hostname() +
        ' in ' +
        process.env.NODE_ENV +
        ' environement using Node ' +
        process.versions.node;
    if (process.env.NODE_ENV === 'development') {
        winston.info(summary);
    }
    winston.info('Good luck...');
}

db.query("SELECT table_name FROM information_schema.tables WHERE table_schema='student_forecast'")
.then(tables => {
    tableString = tables.map(table => table.table_name).join(', ');
    winston.info('Database connection successful');
    winston.info('Tables in student_forecast schema: ' + tableString);
})
.catch(error => {
    winston.error('Error connecting to the database or missing student_forecast schema', error);
});

serverRestartTrainingJobsFetching();