const express = require("express");
const globals = require("../common/globals");
const router = express.Router();
const { scheduleModelTrainingJob, scheduleModelPredictionJob } = require("../services/schedulerService");
const {
    generateScorePredictionDataFromConfig,
    addStudentPerformanceForRelevantCoursesToData,
    determineStudentStatsFromData
} = require("../services/dataAggregatorService");
const { getColumnValues } = require("../services/dataTransformerService");
const { calculateCorrelation } = require("../services/dataCalculationService");
const { executeMethodValidation } = require("../services/codeRunnerService");
const { getStudentScores } = require("../services/dataGathererService");
//const { printTxtTable, saveToCSV } = require('../services/dataViewService');

router.post('/calculate-course-correlations', async (req, res) => {
    const config = req.body;

    const scorePredictionData = await generateScorePredictionDataFromConfig(config, 0);
    const numberOfStudents = scorePredictionData.length;
    //printTxtTable(scorePredictionData, 'table');
    const relevantCoursesData = config.selectedCourseCorrelationAdditionalFeaturesCourses

    const correlationMetrics = relevantCoursesData.map(relevantCourse => ({
        course_acronym: relevantCourse.course_acronym,
        students_with_performance_score_0: 0
    }));

    const scorePredictionDataWithRelevanCoursesPerformances =
        await addStudentPerformanceForRelevantCoursesToData(
            config.courseId,
            scorePredictionData,
            relevantCoursesData
        );

    // Count the number of zero values for each relevant course performance column
    for (const student of scorePredictionDataWithRelevanCoursesPerformances) {
        for (const item of correlationMetrics) {
            const columnName = item.course_acronym + globals.CORRELATION_COURSE_PERFORMANCE_FEATURE_SUFFIX;
            if (student[columnName] === 0) {
                item.students_with_performance_score_0++;
            }
        }
    }

    //printTxtTable(scorePredictionDataWithRelevanCoursesPerformances, 'table');
    //saveToCSV(scorePredictionDataWithRelevanCoursesPerformances, 'table');
    //correlationMetrics.numberOfStudents = numberOfStudents;
    const scores = getColumnValues(scorePredictionDataWithRelevanCoursesPerformances, globals.SCORE_FEATURE);
    for (const relevantCourseMetric of correlationMetrics) {
        const acronym = relevantCourseMetric.course_acronym;
        const performances = getColumnValues(scorePredictionDataWithRelevanCoursesPerformances, acronym + globals.CORRELATION_COURSE_PERFORMANCE_FEATURE_SUFFIX);
        const correlation = calculateCorrelation(scores, performances);
        relevantCourseMetric.correlation = correlation;
    }

    res.json(
        {
            numberOfStudents: numberOfStudents,
            correlationMetrics: correlationMetrics,
            suggestedCoursesToKeepByAcronym: correlationMetrics.filter(metric => metric.correlation >= globals.MIN_CORRELATION_THRESHOLD).map(metric => metric.course_acronym)
        }
    );
});

router.post('/calculate-student-success-training-data-stats', async (req, res) => {
    const config = req.body;
    const scorePredictionData = await generateScorePredictionDataFromConfig(config, 0);

    const stats = determineStudentStatsFromData(scorePredictionData);

    res.json(stats);    
});

router.post("/schedule-training", async (req, res) => {
    const { courseId, academicYearId, predictionPointOrdinal, scheduleTime } = req.body;

    await scheduleModelTrainingJob(courseId, academicYearId, predictionPointOrdinal, scheduleTime);

    res.json({ message: "Training scheduled for " + scheduleTime });
})

router.post("/schedule-prediction", async (req, res) => {
    const { courseId, academicYearId, predictionPointOrdinal, scheduleTime } = req.body;

    await scheduleModelPredictionJob(courseId, academicYearId, predictionPointOrdinal, scheduleTime);

    res.json({ message: "Prediction scheduled for " + scheduleTime });
})

router.post("/validate-method", async (req, res) => {
    const { codeString, type } = req.body;

    const { errors_present, error_message, runtime_message } = await executeMethodValidation(codeString, type);

    res.json({
        methodIsValid: !errors_present,
        validationErrorMessage: error_message,
        runtimeErrorMessage: runtime_message
    });
})

module.exports = router;