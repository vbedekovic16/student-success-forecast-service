# Use Miniconda base image
FROM continuumio/miniconda3

RUN apt-get update && \
    apt-get install -y curl tzdata && \
    curl -sL https://deb.nodesource.com/setup_20.x | bash - && \
    apt-get install -y nodejs

# Set the timezone to Central European Time (CET)
ENV TZ=Europe/Berlin

# Verify installations
RUN node --version
RUN npm --version

WORKDIR /usr/src/app

COPY . .

RUN npm install

# Create the R environment using conda
ENV CONDA_DEFAULT_TIMEOUT=100
RUN conda create -n student-predictions-env -y -c r -c conda-forge \
    r-base64enc r-jsonlite r-dplyr r-caret r-e1071 r-nnet r-xgboost r-randomforest r-torch

# Activate the environment and finish torch installation, then install brulee
RUN echo "library(torch)" > install_torch.R && \
    echo "install_torch()" >> install_torch.R && \
    echo "install.packages('brulee', repos='https://cloud.r-project.org')" >> install_torch.R && \
    /bin/bash -c "source activate student-predictions-env && Rscript install_torch.R"

EXPOSE 4242

CMD ["npm", "run", "prod"]